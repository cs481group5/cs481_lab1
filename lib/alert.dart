//bare bones for the alert dialog
// needs: custom text

import 'package:flutter/material.dart';
import 'package:lab1_project/localization/localization.dart';
//import 'generated/l10n.dart';       //for localization?

class Alert extends StatefulWidget {
  @override
  _AlertState createState() => _AlertState();
}

class _AlertState extends State<Alert> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AlertDialog(
        title: Transform.scale(
          scale: 3,
          child: Icon(Icons.warning, color: Colors.red)
        ),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text(Localization.of(context).getTranslation('alert_title'), textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
              Text(Localization.of(context).getTranslation('alert_box'), textAlign: TextAlign.center),
              SizedBox(height: 10,),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text(Localization.of(context).getTranslation('alert_close')),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }
}