import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:lab1_project/localization/localization.dart';


class Cupertino extends StatefulWidget {
  @override
  _CupertinoState createState() => _CupertinoState();
}

class _CupertinoState extends State<Cupertino> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CupertinoAlertDialog(
        title: Text(Localization.of(context).getTranslation('cupertino_title')),
        content: SingleChildScrollView(
          child: ListBody(
          children: <Widget>[
            Text(Localization.of(context).getTranslation('cupertino_box')),
            SizedBox(height: 10),
            Icon(Icons.mail, color: Colors.grey),
          ],
        ),
      ),
    actions: <Widget>[
      FlatButton(
        child: Text(Localization.of(context).getTranslation('cupertino_close')),
        onPressed: (){
          Navigator.of(context).pop();
        },
      )
    ],
      ),
    );
  }
}
